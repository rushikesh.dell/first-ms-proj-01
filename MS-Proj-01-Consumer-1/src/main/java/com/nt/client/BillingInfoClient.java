package com.nt.client;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class BillingInfoClient {
	//Has a relation
	@Autowired
	private DiscoveryClient client;
	//test comment
	
	//testing for sourcetree
	
	//define method to call consumer application
	public String getBillingInfo() {
		
		//get billing service instance from eureka server
		List<ServiceInstance> listInstance = client.getInstances("Billing-Service");
	
		//get one instance
		ServiceInstance instance = listInstance.get(0);
		
		//get url from instance
		URI uri = instance.getUri();
		
		//prepare url to consume the method
		String serviceUrl = uri+"/producer/api/info";
	
		//create rest template
		RestTemplate template = new RestTemplate();
		
		//call method
		ResponseEntity<String> response = template.getForEntity(serviceUrl, String.class);
		
		//return  
		return response.getBody();
	}
	
}
