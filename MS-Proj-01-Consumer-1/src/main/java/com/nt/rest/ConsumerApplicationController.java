package com.nt.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nt.client.BillingInfoClient;

@RestController
@RequestMapping("/consumer/api")
public class ConsumerApplicationController {
	
	@Autowired
	private BillingInfoClient client;
	
	@GetMapping("/details")
	public ResponseEntity<String> getBillingDetails(){
		//call service method
		String details = client.getBillingInfo();
		//return 
		return new ResponseEntity<String>("bill is : $5000"+details,HttpStatus.OK);
	}
	
	
}
