package com.nt;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class RestControllerOperations {
	
	//define local properties
	@Value("${db.user}")
	private String dbuser;
	@Value("${db.password}")
	private String dbpwd;
	
	//define method
	@GetMapping("/show")
	public ResponseEntity<String>showDetails(){
		//return 
		return new ResponseEntity<String>("DB USER DETAILS FROM CONFIG FILE "+dbuser+"->"+dbpwd,HttpStatus.OK);
	}
	
}
